import sys
ip = ""
port = ""
xy = "socks5"
id = "2"
uid = "532937"
import random
import time
from selenium.webdriver import ChromeOptions, Chrome
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
import requests
import socket

def get_host_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()

    return ip


def input_dependence(ip, port, xy):
    global driver
    opt = ChromeOptions()
    opt.headless = True
    opt.add_argument('--no-sandbox')
    opt.add_argument("--mute-audio")
    opt.add_argument("--disable-gpu")
    opt.add_argument("--disable-software-rasterizer")
    opt.add_experimental_option('excludeSwitches', ['enable-logging'])
    opt.add_experimental_option('useAutomationExtension', False)
    opt.add_argument('--disable-blink-features=AutomationControlled')
    driver = Chrome(options=opt)
    driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {"source": "Object.defineProperty(navigator, 'webdriver', {get: () => undefined})"})
    driver.set_page_load_timeout(60)
    return driver

def run(ip, port, xy, uid, id):
    global driver, count
    while True:
        driver.get(f"https://radioearn.com/radio/{id}/?uid={uid}")
        try:
            WebDriverWait(driver, 6, 1).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'body > center > font')))
            c = driver.find_element(By.CSS_SELECTOR, 'body > center > font').text
            if "wait" in c:
                time.sleep(random.randint(61, 70))
                driver.get(f"https://radioearn.com/radio/{id}/?uid={uid}")
            time.sleep(6)
        except:
            time.sleep(6)
            pass
        
        try:
            driver.find_element(By.XPATH, '/html/body/div[1]/a/img')
        except:
            pass
        Ac = ActionChains(driver)
        Ac.send_keys(Keys.TAB).perform()
        Ac.send_keys(Keys.TAB).perform()
        Ac.send_keys(Keys.TAB).perform()
        time.sleep(0.3)
        Ac.send_keys(Keys.ENTER).perform()
        while True:
            time.sleep(5 * 61)
            try:
                res = requests.get("https://radioearn.com/api/get.php?uid=532937&api=BWXlL6MI5koslOfdCf&out=3").text
                if f"1 | Radio 2 | {str(get_host_ip())}" not in res:
                    count += 1
                    break
                else:
                    count = 0
                    time.sleep(10 * 61)
                if count > 8:
                    close_driver()
                    sys.exit()
            except:
                pass

def main():
    global count
    input_dependence(ip, port, xy)
    count = 0
    run(ip, port, xy, uid, id)
    close_driver()

def close_driver():
    global driver, c_service
    try:
        driver.quit()
    except Exception as e:
        print(e)

if __name__ == '__main__':
    main()
